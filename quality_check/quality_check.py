from __future__ import print_function

import urllib
import os
import shutil
import datetime
import boto3
import botocore
import zlib
import gzip
import sys
import json
import re
import math
import time

# Non standard libraries packaged in ./lib directory
lib = os.path.abspath('lib/')
sys.path.append(lib)

# Non standard libraries
from bs4 import BeautifulSoup

from RinexFileValidation import RinexFileValidation
from JsEncoder import JSEncoder

from rinex_data import *
from executable import Executable
from elasticsearch import Elasticsearch, RequestsHttpConnection
from aws_requests_auth.aws_auth import AWSRequestsAuth

S3 = boto3.client('s3')

def foo(text,context):
    return('{}'.format(text))
    
def getFileNames(event,context):
    
    bucket = event['Records'][0]['s3']['bucket']['name']	
    startDate = urllib.unquote_plus(
        event['Records'][0]['s3']['object']['startDate']).decode('utf8')
    endDate = urllib.unquote_plus(
        event['Records'][0]['s3']['object']['endDate']).decode('utf8')
	
    fileNames = []
    for key in S3.list_objects(Bucket=bucket)['Contents']:

        if 'MO.crx.' in ''.join([key['Key']]):
            fileNames.append([key['Key']])
        if 'MN.rnx.' in ''.join([key['Key']]):
            continue
    return fileNames

    
def lambda_handler(event, context):
    # Get the file object and bucket names from the event
    bucket = event['Records'][0]['s3']['bucket']['name']
    key = urllib.unquote_plus(
        event['Records'][0]['s3']['object']['key']).decode('utf8')
        
    rnx2Pattern=re.match('[a-z]{4}[0-9]{4}.[0-9]{2}d.gz', key) 
    if rnx2Pattern not in ['None']:
        station = key[0:4]
        year=key[9:11]   
        day=key[4:7]        
    else:
        station, dataSource, sttimeString, filePeriod, dataFreq, contentFormatCompression = key.split('_')[:6]
        year=sttimeString[:4]
        day=sttimeString[5:7]

    is_nav_file = os.path.basename(key)
    if is_nav_file == 'brdc':
        nav_file = os.path.basename(key)
        if nav_file[:4].lower() == 'brdc' and nav_file[-4:] == 'n.gz':
            triggerQCFromNav(year, day, context.function_name, bucket)

        else:
            print('Do not Quality Check using non-Broadcast Navigation data')

        return

    # Use AWS request ID from context object for unique directory
    session_id = context.aws_request_id
    local_path = '/tmp/{}'.format(session_id)

    if not os.path.exists(local_path):
        os.makedirs(local_path)

    try:
        response = S3.get_object(Bucket=bucket, Key=key)

    except Exception as err:
        # This should only fail more than once if permissions are incorrect
        print('Error: Failed to get object {} from bucket {}.'.format(
            key, bucket))
        raise err

    # Decompress Observation file and store locally
    filename, extension = os.path.splitext(os.path.basename(key))
    local_file = os.path.join(local_path, filename)

    file_data = zlib.decompress(response['Body'].read(), 15+32)
    with open(local_file, 'wb') as out_file:
        out_file.write(file_data)

    # Parse RINEX file
    rinex_obs = RINEXData(local_file)

    # Hatanaka decompress RINEX file if needed 
    if rinex_obs.compressed == True:
        rinex_obs.local_file = hatanaka_decompress(rinex_obs.local_file)

    # Generate an Anubis XML config file
    anubis_config, result_file = generateQCConfig(
        rinex_obs, '', local_path)

    # Run Anubis with the generated config file as input
    anubis = Executable('lib/executables/anubis-2.1.3')
    anubis_log = anubis.run('-x {}'.format(anubis_config))
    
    # Following section is closed as full header check is not possible due to issues
    
    if anubis.returncode > 0:
        print('Anubis errored with return code {}: {}\n{}'.format(
            anubis.returncode, anubis.stderr, anubis.stdout))
    try: 
        # Parse results of Anubis
        parseQCResult(result_file, key,1)
    except:
        raise
        
    

    # Delete tmp working space and Anubis copy to resolve Lambda disk 
    # space allocation issue
    shutil.rmtree(local_path)

    return


def hatanaka_decompress(local_file):
    """Hatanaka decompresses a local file using the CRX2RNX program
    Outputs data to new file with correct name under same directory as input

    Input:
        local_file  path to Hatanaka compressed RINEX file

    Returns:
        new_name    name of created decompressed RINEX file
    """
    # Check if CRX2RNX is in /tmp - where Lambda instances are throttled
    if os.path.isfile('/tmp/CRX2RNX'):
        CRX2RNX = Executable('/tmp/CRX2RNX', True)

    else:
        CRX2RNX = Executable('lib/executables/CRX2RNX')

    rinex_data = CRX2RNX.run('{} -'.format(local_file))

    if CRX2RNX.returncode > 0:
        raise Exception('CRX2RNX failed with error code {}: {}'.format(
            CRX2RNX.returncode, CRX2RNX.stderr))

    # RINEX 3 file extension changes from crx to rnx when decompressed
    new_name = local_file.replace('.crx', '.rnx')
    
    # Hatanaka compressed RINEX 2 files are suffixed with d, replace with o
    if new_name == local_file:
        new_name = local_file[:-1] + 'o'

    with open(new_name, 'w') as out_file:
        out_file.write(rinex_data)

    return new_name


def getBRDMNavFile(bucket, date, out_dir):
    """Attempts to get the daily BRDC Nav file for a given date from archive

    Broadcast Navigation file always has prefix public/daily/nav/<year>/<day>/
    Filename is always 'brdc..'
    """
    year, day = date.strftime('%Y-%j').split('-')
    brdc = 'brdm{}0.{}p.gz'.format(day, year[2:])
    
    try:
        response = S3.get_object(Bucket=bucket, Key=brdc)

    except botocore.exceptions.ClientError as err:
        if err.response['Error']['Code'] == 'NoSuchKey':
            # BRDC File does not yet exist, do nothing
            return

        raise

    out_file = os.path.join(out_dir, os.path.basename(brdc))
    # Decompress Nav data, must be .gz file given the GET request asks for one
    nav_data = zlib.decompress(response['Body'].read(), 15+32)
    
    with open(out_file, 'w') as output:
        output.write(nav_data)

    return out_file


def generateQCConfig(rinex_obs, nav_file, output_dir):
    """Generates Anubis configuration file given the following: 

    Input:
        rinex_obs       RINEXData object
        nav_file        path to corresponding Navigation file
        output_dir      directory for quality output file

    Returns:
        config_file     file containing generated config file
        results_file    file which Anubis will output results to
    """
    base = BeautifulSoup(open('anubis_base.cfg'),"html.parser")

    # Currently assuming that start and end time are start and end of day
    base.config.gen.beg.contents[0].replaceWith('"{}"'.format(
        rinex_obs.start_time.strftime('%Y-%m-%d 00:00:00')))

    base.config.gen.end.contents[0].replaceWith('"{}"'.format(
        rinex_obs.start_time.strftime('%Y-%m-%d 23:59:59')))

    base.config.gen.rec.contents[0].replaceWith(rinex_obs.marker_name)

    base.config.inputs.rinexo.contents[0].replaceWith(rinex_obs.local_file)

    base.config.inputs.rinexn.contents[0].replaceWith(nav_file)

    results_file = os.path.join(output_dir, 'output.xml')
    base.config.outputs.xml.contents[0].replaceWith(results_file)

    config_file = os.path.join(output_dir, 'config.xml')
    with open(config_file, 'w') as out:
        out.write(base.prettify('utf-8'))
    
    return config_file, results_file


def anubisBucket(event, context):
    
    key = urllib.unquote_plus(
         event['Records'][0]['s3']['object']['key']).decode('utf8')
    bucket = event['Records'][0]['s3']['bucket']['name']	
    
    session_id = context.aws_request_id
    fileNames = []
    for key in S3.list_objects(Bucket=bucket)['Contents']:
        fileNames += [key['Key']]
        fname=key['Key'];
        #print(key['Key'])
        if 'MN.rnx.' in ''.join([key['Key']]):
            # meteorological data processing
            metFileProcess(bucket,fname,session_id)    
            continue
        
        if '.Z' in ''.join([key['Key']]):
            continue
            
        # nav file is not required
        if 'p.gz' in ''.join([key['Key']]):
            continue    
            
        if '.gz' not in ''.join([key['Key']]):
            continue    
    
        #try:
        rnx2Pattern=re.match('[a-z]{4}[0-9]{4}.[0-9]{2}d.gz', fname) # also accepts only rinex 2 observation file
        if rnx2Pattern not in ['None']:
            station = fname[0:4]
            year=fname[9:11]   
            day=fname[4:7]        
        else:
            station, dataSource, sttimeString, filePeriod, dataFreq, contentFormatCompression = fname.split('_')[:6]
            year=sttimeString[:4]
            day=sttimeString[5:7]

    
            
        data_type='';
    return fileNames
	
	
def anubisWithFilename(bucket,Ky,year,day,data_type,session_id):
    # Get the file object and bucket names from the event
    nav_file='';
    local_path = '/tmp/{}'.format(session_id)
    
    if not os.path.exists(local_path):
        os.makedirs(local_path)
    try:
        response = S3.get_object(Bucket=bucket, Key=Ky)

    except Exception as err:
        # This should only fail more than once if permissions are incorrect
        print('Error: Failed to get object {} from bucket {}.'.format(
            Ky, bucket))
        raise err

    # Decompress Observation file and store locally
    filename, extension = os.path.splitext(os.path.basename(Ky))
    local_file = os.path.join(local_path, filename)
    
    
    file_data = zlib.decompress(response['Body'].read(), 15+32)
    with open(local_file, 'wb') as out_file:
        out_file.write(file_data)

    # Parse RINEX file
    rinex_obs = RINEXData(local_file)

    # Attempt to get Broadcast Navigation file from archive
    nav_file = ''
    
    # Hatanaka decompress RINEX file if needed 
    if rinex_obs.compressed == True:
        rinex_obs.local_file = hatanaka_decompress(rinex_obs.local_file)

    # Generate an Anubis XML config file
    anubis_config, result_file = generateQCConfig(
        rinex_obs, nav_file, local_path)

    # Run Anubis with the generated config file as input
    anubis = Executable('lib/executables/anubis-2.1.3')
    anubis_log = anubis.run('-x {}'.format(anubis_config))
    
    
           
    if anubis.returncode > 0:
        print('Anubis errored with return code {}: {}\n{}'.format(
            anubis.returncode, anubis.stderr, anubis.stdout))
            #return
    
    # Parse results of Anubis
    try: 
        parseQCResult(result_file, Ky,2)
    except:
        raise

    # Delete tmp working space and Anubis copy to resolve Lambda disk 
    # space allocation issue
    shutil.rmtree(local_path)
    
    

def parseQCResult(filename, key, qcFlag):
    """Extract relevant QC metrics from Anubis output file and store in 
    ElasticSearch

    Only takes key for inclusion in ES documents
    """
    # Create authenticated connection to Elasticsearch cluster
    es_host = 'search-gnss-datacenter-es-2a65hbml7jlcthde6few3g7yxi.ap-southeast-2.es.amazonaws.com'
    es_index = 'quality_metrics'

    cred = boto3.session.Session().get_credentials()
    auth = AWSRequestsAuth(
        aws_access_key=cred.access_key,
        aws_secret_access_key=cred.secret_key,
        aws_token=cred.token,
        aws_host=es_host,
        aws_region='ap-southeast-2',
        aws_service='es')

    es_client = Elasticsearch(
        host=es_host,
        port=80,
        connection_class=RequestsHttpConnection,
        http_auth=auth)

    # Read results XML file
    results = BeautifulSoup(open(filename),"html.parser")

    # Extract header information from results file to validate against truth
    # greb the truth
    
    stime=datetime.datetime.strptime(results.qc_gnss.data.time_beg.contents[0], '%Y-%m-%dT%H:%M:%S')
    
    timeStamp_beginning=isoTimetotrunckatedTime(results.qc_gnss.data.time_beg.contents[0]);
    timeStamp_end=isoTimetotrunckatedTime(results.qc_gnss.data.time_end.contents[0]);
    
    url="https://gws.geodesy.ga.gov.au/setups/search/findByFourCharacterId?id=" + results.qc_gnss.head.site_id.contents[0] \
            + "&effectiveFrom=" + timeStamp_beginning + "&effectiveTo=" + timeStamp_end + "&timeFormat=uuuu-MM-dd"
    
    
    # For Site validation    
    # due to the fact that return from url object does not contain all the site specific parameters like: site approximate position 
    response = urllib.urlopen('https://gws.geodesy.ga.gov.au/siteLogs/search/findByFourCharacterId?id='  + results.qc_gnss.head.site_id.contents[0] + '&format=geodesyml')
    siteXML = response.read()
    resultsSiteXML = BeautifulSoup(siteXML,"html.parser")
    RFV = RinexFileValidation(False,False,False)
    RFV = rinexSiteValidation(results,resultsSiteXML,RFV)
    
    # For receiver and antenna validation as it comes as  json object
    response = urllib.urlopen(url)
    data = json.loads(response.read())
    siteLogTruth = fromatJSONObjecttoQC(data)
    
    RFV = rinexReceiverValidationJSON(results,siteLogTruth,RFV)
    RFV = rinexAntennaValidationParametersJSON(results,siteLogTruth,RFV)
    
    
    if qcFlag == 1: #only metadata check
        return RFV
    
    
    # Extract relevant fields from results file
    for system in results.qc_gnss.data.findAll('system'):
        # assumtion is QC will be only for daily file
        # dailyRinexFileCompletenss is based on the interval from the QCed header and daily files

        expectedEpochs=int(60/float(results.qc_gnss.data.data_int.string)*60*24)
        haveEpochs=int(system.epo.have.string)
        # in percentage rounded to nearest threedecimal place due to check the availability for
        # 99.999% data availibility       
        dailyRinexFileCompletenss=round(float(int(haveEpochs)/float(expectedEpochs))*100,3)
        
        timeStamp=int(time.mktime(datetime.datetime.strptime(results.qc_gnss.data.time_beg.contents[0], '%Y-%m-%dT%H:%M:%S').timetuple()))
        log('DailyRinexFileCompletenss', timeStamp, 'gauge',float(dailyRinexFileCompletenss), ['site_id:' + str(results.qc_gnss.head.site_id.contents[0]) + ',gnssSystem:' +str(system['type'])])    
        for obs in system.findAll('obs'):
            
            doc = {
                'site_id': results.qc_gnss.head.site_id.contents[0],
                'system': system['type'],
                'timestamp': datetime.datetime.strptime(
                    results.qc_gnss.data.time_beg.contents[0], '%Y-%m-%dT%H:%M:%S'),
                'file_type': 'daily',
                'filename': key
            }
            for attribute, value in obs.attrs:
                if attribute == 'type':
                    try:
                        _type, band, doc['attribute'] = value

                    except ValueError:
                        _type, band = value
                        doc['attribute'] = None

                    doc['band'] = int(band)

                else:
                    attr_name, attr_type = attribute_map[attribute]
                    doc[attr_name] = attr_type(value)

            if _type not in ['L', 'C']:
                # Only want to store Pseudoranges and Codes
                continue
            doc['expected_obs'] = int(obs.expt.string) if obs.expt.string not in 'n/a'  else 0
            doc['have_obs'] = int(obs.have.string) if obs.have.string not in 'n/a'  else 0
            doc['expected_obs_10_degrees'] = int(obs.expt_usr.string) if obs.expt_usr.string not in 'n/a'  else 0
            doc['have_obs_10_degrees'] = int(obs.have_usr.string) if obs.have_usr.string not in 'n/a'  else 0
            doc['number_sat'] = int(obs.nsat.string) if obs.nsat.string not in 'n/a'  else 0
            
            if _type in ['L']:
                doc['cycle_slips'] = int(obs.slps.string) if obs.slps.string not in 'n/a'  else 0
            elif _type in ['C']:
                doc['multipath'] = float(obs.mpth.string) if obs.mpth.string not in 'n/a'  else 0

                
                
            # Convert type to full name, L=phase C=code
            types = {'L': 'phase', 'C': 'code'}

            # ID is composite field so that reprocessed files will overwrite old data
            doc_id = '{}{}{}{}{}{}'.format(
                doc['site_id'], doc['system'], doc['timestamp'], 
                doc['file_type'], doc['band'], doc['attribute'])
                
            submitElasticSearchCluster('quality_metrics',doc_id,doc)
    return RFV

def log(metric_name,timestamp, metric_type='count', metric_value=1, tags=[]):
    # format:
    # MONITORING|unix_epoch_timestamp|metric_value|metric_type|my.metric.name|#tag1:value,tag2
    #print("MONITORING|{}|{}|{}|{}|#{}".format(
    #   timestamp, metric_value, metric_type, metric_name, ','.join(tags)
    #))    
    print("MONITORING|{}|{}|{}|{}|#{}".format(
       int(time.time()), metric_value, metric_type, metric_name, ','.join(tags)
    ))    

def triggerQCFromNav(year, day, function_name, bucket):
    """Invoke a lambda function with PUT operations for all objects in archive 
    for a given year and day of year
    """
    lambda_client = boto3.client('lambda')
    # Define base trigger parameters
    lambdaTriggerParams = {
        "Records": [
            {
                "eventTime": datetime.datetime.utcnow().isoformat() + 'Z',
                "s3": {
                    "object": {
                        "key": "",
                    },
                    "bucket": {
                        "name": "",
                    },
                    "s3SchemaVersion": "1.0"
                },
                "awsRegion": "ap-southeast-2",
                "eventName": "ObjectCreated:Put",
                "eventSource": "aws:s3"
            }
        ]
    }

    # Loop through all public and private daily obs files for given day
    prefix = '/daily/obs/{}/{}/'.format(year, day)
    for s3_obj in getKeys(bucket, ['public' + prefix, 'private' + prefix]):
        request = lambdaTriggerParams
        request['Records'][0]['s3']['object']['key'] = s3_obj
        request['Records'][0]['s3']['bucket']['name'] = bucket

        try:
            # Attempt to invoke lambda function
            lambda_client.invoke_async(
                FunctionName=function_name,
                InvokeArgs=json.dumps(request))

        except Exception as err:
            print('Invocation of {} Lambda failed for key {}\n{}'.format(
                function_name, s3_obj, err))
            pass


def getKeys(bucket, prefixes):
    """Get a list of S3 objects in a bucket with a given prefix

    Paginates responses in case there are over 1000 entries

    prefixes can be single prefix, or list of prefixes
    """
    if type(prefixes) is not list:
        prefixes = [prefixes]

    keys = []
    for prefix in prefixes:
        paginator = S3.get_paginator('list_objects')
        page_iterator = paginator.paginate(Bucket=bucket, Prefix=prefix)

        for page in page_iterator:
            if 'Contents' in page:
                keys += [obj['Key'] for obj in page['Contents']]

    return keys

def rinexSiteValidation(results,resultsSiteXML,RFV):
    #site validation
    
    # Cartesian position     
    resultsSiteXMLPos=resultsSiteXML.find('gml:pos')
    resultsSiteXMLPos=resultsSiteXMLPos.contents[0]
    fromQCPos=results.qc_gnss.head.position.coordinate.find('gml:pos')
    fromQCPos=fromQCPos.contents[0]
    
    if(( not fromQCPos) or ( not resultsSiteXMLPos)):
        print("Empty position")
        return

    posTruth='{}'.format(resultsSiteXMLPos).split(" ")
    posTruth=[float(posTruth[0]), float(posTruth[1]), float(posTruth[2])]
    posQC='{}'.format(fromQCPos).split(" ")
    posQC=[float(posQC[0]), float(posQC[1]), float(posQC[2])]
    
    distance=math.sqrt(sum([(a - b)**2 for a, b in zip(posTruth, posQC)])) # 3D distance in euclidean distance

    print('distance {}'.format(distance))
    
    # distance is excepted less then 15m
    if(distance < 15.00 ):
        RFV.SiteValidationParameters.isSiteCartesianPositionXValid = True
        RFV.SiteValidationParameters.isSiteCartesianPositionYValid = True
        RFV.SiteValidationParameters.isSiteCartesianPositionZValid = True
    else:
        print("Doomed:isSiteCartesianPosition")
        print('resultsSiteXMLPos: {}'.format(resultsSiteXMLPos))
        print('fromQCPos: {}'.format(fromQCPos))

    
    # four char ID
    # from truth
    fourCharID=resultsSiteXML.find('geo:fourcharacterid')
    fourCharID=fourCharID.contents[0]
    
    # isSiteAgencyValid is set to truth if all other is validate
    # as we do not have a specific filed in xml for this. agency belongs to sitecontact
    IERSDOMESNumber=resultsSiteXML.find('geo:iersdomesnumber')
    IERSDOMESNumber=IERSDOMESNumber.contents[0]
    
    
    # from QC
    fromQCIERSDOMESNumber=results.qc_gnss.head.marker_numb.contents[0]
    fromQCFourCharID=results.qc_gnss.head.site_id.contents[0]

    
    if(fromQCFourCharID == fourCharID):
        RFV.SiteValidationParameters.isSite4CharNameValid = True
    else:
        print("Doomed:4CharNameValid")
    
    if(fromQCIERSDOMESNumber == IERSDOMESNumber):
        RFV.SiteValidationParameters.isSiteIERSDOMESNumberValid = True
    else:
        print("Doomed:isSiteIERSDOMESNumberValid")
    
    if (    RFV.SiteValidationParameters.isSite4CharNameValid 
        and RFV.SiteValidationParameters.isSiteIERSDOMESNumberValid
        and RFV.SiteValidationParameters.isSiteCartesianPositionXValid
        and RFV.SiteValidationParameters.isSiteCartesianPositionYValid
        and RFV.SiteValidationParameters.isSiteCartesianPositionZValid
        ):
        RFV.isSiteValid = True
        RFV.SiteValidationParameters.isSiteAgencyValid = True

    if (RFV.isSiteValid):
        print('isSiteValid is valid')
    else:
        print('issue')        
        
    return RFV

def rinexReceiverValidationJSON(results,siteLogs,RFV):
    # receiver version firmware validation
    
    #From QC

    if(siteLogs['receiver']['firmwareVersion'] == results.qc_gnss.head.receiver_firmware.contents[0]):
        RFV.ReceiverValidationParameters.isFirmwareValid = True
    else:
        print("Doomed:ReceiverValidationParameters.isFirmwareValid")
    
    if(siteLogs['receiver']['receiverName'] == results.qc_gnss.head.receiver_type.contents[0]):
        RFV.ReceiverValidationParameters.isGnssReceiverNameValid = True
    else:
        print("Doomed:ReceiverValidationParameters.isGnssReceiverNameValid")

    if(siteLogs['receiver']['manufacturerSerialNumber'] == results.qc_gnss.head.receiver_numb.contents[0]):
        RFV.ReceiverValidationParameters.isManufacturerSerialNumberNumberValid = True
    else:
        print("Doomed:ReceiverValidationParameters.isManufacturerSerialNumberNumberValid")

    
    if (    RFV.ReceiverValidationParameters.isFirmwareValid 
       and RFV.ReceiverValidationParameters.isGnssReceiverNameValid
       and RFV.ReceiverValidationParameters.isManufacturerSerialNumberNumberValid
       ):
        RFV.isReceiverValid = True
    
    return RFV
    
def rinexReceiverValidationGEOXML(results,resultsSiteXML,RFV):
    # receiver version firmware validation
 
    # firmware version filed is not present in anubisQC result file
    # if anubis is updated then this method need to be implemented
    
    
    # last gnss receiver
    gnssRec = resultsSiteXML.findAll('geo:gnssreceiver')
    gnssRec=gnssRec[-1]# get the last element of the list
    
    # from truth
    # firmware version
    fmv = gnssRec.findAll('geo:firmwareversion')
    
    
    
    fmv = fmv[-1].contents[0] 
    
    #receiver name
    rnv = gnssRec.findAll('geo:igsmodelcode')
    rnv = rnv[-1].contents[0] 
    
    # Manufacturer Serial NumberNumber
    msn = gnssRec.findAll('geo:manufacturerserialnumber')
    msn = msn[-1].contents[0] 
    
    #From QC
    fromQCIfmv=results.qc_gnss.head.receiver_firmware.contents[0]
    fromQCIrnv=results.qc_gnss.head.receiver_type.contents[0]
    fromQCImsn=results.qc_gnss.head.receiver_numb.contents[0]

    if(fmv == fromQCIfmv):
        RFV.ReceiverValidationParameters.isFirmwareValid = True
    else:
        print("Doomed:ReceiverValidationParameters.isFirmwareValid")

    if(rnv == fromQCIrnv):
        RFV.ReceiverValidationParameters.isGnssReceiverNameValid = True
    else:
        print("Doomed:ReceiverValidationParameters.isGnssReceiverNameValid")

    if(msn == fromQCImsn):
        RFV.ReceiverValidationParameters.isManufacturerSerialNumberNumberValid = True
    else:
        print("Doomed:ReceiverValidationParameters.isManufacturerSerialNumberNumberValid")

    
    if (    RFV.ReceiverValidationParameters.isFirmwareValid 
       and RFV.ReceiverValidationParameters.isGnssReceiverNameValid
       and RFV.ReceiverValidationParameters.isManufacturerSerialNumberNumberValid
       ):
        RFV.isReceiverValid = True
    
    if (RFV.isReceiverValid):
        print('isReceiverValid is valid')
    else:
        print('issue')        
    
    return RFV
    
def rinexAntennaValidationParametersJSON(results,siteLogs,RFV):
    # Antenna Parameters validation
    # isMarkerArp position     
    resultsSiteXMLARP = [ float(siteLogs['antenna']['markerArpUpEcc']),
                            float(siteLogs['antenna']['markerArpNorthEcc']),    
                            float(siteLogs['antenna']['markerArpEastEcc']),
                        ]
    
    fromQCARP =  [ float(results.qc_gnss.head.position.eccentricity.find("axis",{"name":"U"}).contents[0]),
                    float(results.qc_gnss.head.position.eccentricity.find("axis",{"name":"N"}).contents[0]),
                    float(results.qc_gnss.head.position.eccentricity.find("axis",{"name":"E"}).contents[0])
                 ]
    # ARP should be exact to the database....               
    if(resultsSiteXMLARP==fromQCARP):
        RFV.AntennaValidationParameters.isMarkerArpUpEcc = True
        RFV.AntennaValidationParameters.isMarkerArpNorthEcc = True
        RFV.AntennaValidationParameters.isMarkerArpEastEcc = True
    else:
        print("Doomed:AntennaValidationParameterss")
        if(resultsSiteXMLARP[0]==fromQCARP[0]):
            RFV.AntennaValidationParameters.isMarkerArpUpEcc=True
            print("Doomed:AntennaValidationParameterssUP")
        if(resultsSiteXMLARP[1]==fromQCARP[1]):
            RFV.AntennaValidationParameters.isMarkerArpNorthEcc=True
            print("Doomed:AntennaValidationParameterssNorth")
        if(resultsSiteXMLARP[2]==fromQCARP[2]):
            RFV.AntennaValidationParameters.isMarkerArpEastEcc=True
            print("Doomed:AntennaValidationParameterssEast")
        
        
    # Antenna Name
    antennaName=siteLogs['antenna']['type']
    antennaName=antennaName.replace(' ','')
    fromQCAntennaName= results.qc_gnss.head.antenna_type.contents[0]+results.qc_gnss.head.antenna_dome.contents[0]
    if(fromQCAntennaName==antennaName):
        RFV.AntennaValidationParameters.isAntennaName = True
    else:
        print("Antenna Name invalid")
        print("QC:"+fromQCAntennaName + "  Sitelog: "+antennaName)
    
    
    # Antenna Serial Number
    antSerialNo = siteLogs['antenna']['manufacturerserialnumber'];
    fromQCAntennaName=  results.qc_gnss.head.antenna_numb.contents[0]
        
    
    if(fromQCAntennaName == antSerialNo):
        RFV.AntennaValidationParameters.isManufacturerSerialNumber = True
    else:
        print("Doomed:isSiteIERSDOMESNumberValid")
    
    if (    RFV.AntennaValidationParameters.isAntennaName 
       and RFV.AntennaValidationParameters.isManufacturerSerialNumber
       and RFV.AntennaValidationParameters.isMarkerArpUpEcc
       and RFV.AntennaValidationParameters.isMarkerArpNorthEcc
       and RFV.AntennaValidationParameters.isMarkerArpEastEcc
       ):
        RFV.isAntennaValid = True
    
    if (RFV.isAntennaValid):
        print('isAntennaValid is valid')
    else:
        print('issue')        

    return RFV
        

    
    
def rinexAntennaValidationParametersGEOXML(results,resultsSiteXML,RFV):
    # Antenna Parameters validation
    gnssANT = resultsSiteXML.findAll('geo:gnssantenna')
    gnssANT=gnssANT[-1]
    
    
    # isMarkerArp position     
    
    resultsSiteXMLARP = [ float(gnssANT.find('geo:marker-arpupecc.').contents[0]),
                            float(gnssANT.find('geo:marker-arpnorthecc.').contents[0]),
                            float(gnssANT.find('geo:marker-arpeastecc.').contents[0])
                        ]
    
    fromQCARP =  [ float(results.qc_gnss.head.position.eccentricity.find("axis",{"name":"U"}).contents[0]),
                    float(results.qc_gnss.head.position.eccentricity.find("axis",{"name":"N"}).contents[0]),
                    float(results.qc_gnss.head.position.eccentricity.find("axis",{"name":"E"}).contents[0])
                 ]
    # ARP should be exact to the database....               
    if(resultsSiteXMLARP==fromQCARP):
        RFV.AntennaValidationParameters.isMarkerArpUpEcc = True
        RFV.AntennaValidationParameters.isMarkerArpNorthEcc = True
        RFV.AntennaValidationParameters.isMarkerArpEastEcc = True
    else:
        print("Doomed:AntennaValidationParameterss")
        if(resultsSiteXMLARP[0]==fromQCARP[0]):
            RFV.AntennaValidationParameters.isMarkerArpUpEcc=True
            print("Doomed:AntennaValidationParameterssUP")
        if(resultsSiteXMLARP[1]==fromQCARP[1]):
            RFV.AntennaValidationParameters.isMarkerArpNorthEcc=True
            print("Doomed:AntennaValidationParameterssNorth")
        if(resultsSiteXMLARP[2]==fromQCARP[2]):
            RFV.AntennaValidationParameters.isMarkerArpEastEcc=True
            print("Doomed:AntennaValidationParameterssEast")
        
        
    # Antenna Name
    antennaName=gnssANT.find("geo:igsmodelcode").contents[0]
    antennaName=antennaName.replace(' ','')

    fromQCAntennaName= results.qc_gnss.head.antenna_type.contents[0]+results.qc_gnss.head.antenna_dome.contents[0]
    
    if(fromQCAntennaName==antennaName):
        RFV.AntennaValidationParameters.isAntennaName = True
    else:
        print("Doomed:isAntennaName")
    
    
    # Antenna Serial Number
    antSerialNo= gnssANT.find('geo:manufacturerserialnumber');
    antSerialNo=antSerialNo.contents[0]
    
    fromQCAntennaName=  results.qc_gnss.head.antenna_numb.contents[0]
        
    
    if(fromQCAntennaName == antSerialNo):
        RFV.AntennaValidationParameters.isManufacturerSerialNumber = True
    else:
        print("Doomed:isSiteIERSDOMESNumberValid")
    
    if (    RFV.AntennaValidationParameters.isAntennaName 
       and RFV.AntennaValidationParameters.isManufacturerSerialNumber
       and RFV.AntennaValidationParameters.isMarkerArpUpEcc
       and RFV.AntennaValidationParameters.isMarkerArpNorthEcc
       and RFV.AntennaValidationParameters.isMarkerArpEastEcc
       ):
        RFV.isAntennaValid = True
    
    if (RFV.isAntennaValid):
        print('isAntennaValid is valid')
    else:
        print('issue')        
    return RFV
        
        
        
        
def qc_handler(file, context):
    # Get the file object and bucket names from the event
    
    key=file['fileName']
    flag=file['qc']
    bucket =file['bucket'] # "test-maz"
    
    print('Quality Check: Key {}'.format(key))
    print('Quality Check: flag {}'.format(flag))
    print('Quality Check: bucket {}'.format(bucket))
    rnx2Pattern=re.match('[a-z]{4}[0-9]{4}.[0-9]{2}d.gz', key) # also accepts only rinex 2 observation file
    if rnx2Pattern != None:
        station = key[0:4]
        year=key[9:11]   
        day=key[4:7]        
    else:
        rinex3Obspattern=re.match(r'(.*).crx.gz', key)
        if rinex3Obspattern == None:
            print('Error: is not a valid file name {}.'.format(key))
            raise err
        station, dataSource, sttimeString, filePeriod, dataFreq, contentFormatCompression = key.split('_')[:6]
        year=sttimeString[:4]
        day=sttimeString[5:7]
    
    is_nav_file = os.path.basename(key)
    if is_nav_file == 'brdc':
        nav_file = os.path.basename(key)
        if nav_file[:4].lower() == 'brdc' and nav_file[-4:] == 'n.gz':
            triggerQCFromNav(year, day, context.function_name, bucket)

        else:
            print('Do not Quality Check using non-Broadcast Navigation data')

        return

    # Use AWS request ID from context object for unique directory
    session_id = context.aws_request_id
    local_path = '/tmp/{}'.format(session_id)

    if not os.path.exists(local_path):
        os.makedirs(local_path)

    try:
        response = S3.get_object(Bucket=bucket, Key=key)

    except Exception as err:
        # This should only fail more than once if permissions are incorrect
        print('Error: Failed to get object {} from bucket {}.'.format(
            key, bucket))
        raise err

    # Decompress Observation file and store locally
    filename, extension = os.path.splitext(os.path.basename(key))
    local_file = os.path.join(local_path, filename)

    file_data = zlib.decompress(response['Body'].read(), 15+32)
    with open(local_file, 'wb') as out_file:
        out_file.write(file_data)

    # Parse RINEX file
    rinex_obs = RINEXData(local_file)
    nav_file = ""
    
    # Hatanaka decompress RINEX file if needed 
    if rinex_obs.compressed == True:
        rinex_obs.local_file = hatanaka_decompress(rinex_obs.local_file)

    # Generate an Anubis XML config file
    anubis_config, result_file = generateQCConfig(
        rinex_obs, nav_file, local_path)

    # Run Anubis with the generated config file as input
    anubis = Executable('lib/executables/anubis-2.1.3')
    anubis_log = anubis.run('-x {}'.format(anubis_config))
    
    # Following section is closed as full header check is not possible due to issues
    if anubis.returncode > 0:
        print('Anubis errored with return code {}: {}\n{}'.format(
            anubis.returncode, anubis.stderr, anubis.stdout))
    try: 
        # Parse results of Anubis
        if flag == "full":
          RFV=parseQCResult(result_file, key, 2) #for detail check and submit to elastic search
        else:
          RFV=parseQCResult(result_file, key, 1) #only metadata check
    except:
        raise

    # Delete tmp working space and Anubis copy to resolve Lambda disk 
    # space allocation issue
    shutil.rmtree(local_path)

    return JSEncoder().encode(RFV)

    
def qc_handler_EventStoreLink(file, context):
    
    
    rinexFilename=file['rinexFilename']
    key=file['key']
    bucket =file['bucket']
    flag=file['qc']
    
    # Use AWS request ID from context object for unique directory
    session_id = context.aws_request_id
    local_path = '/tmp/{}'.format(session_id)

    if not os.path.exists(local_path):
        os.makedirs(local_path)

    try:
        response = S3.get_object(Bucket=bucket, Key=key)

    except Exception as err:
        # This should only fail more than once if permissions are incorrect
        print('Error: Failed to get object {} from bucket {}.'.format(
            key, bucket))
        raise err

    # Decompress Observation file and store locally
    filename, extension = os.path.splitext(os.path.basename(rinexFilename))
    local_file = os.path.join(local_path, filename)

    file_data = zlib.decompress(response['Body'].read(), 15+32)
    with open(local_file, 'wb') as out_file:
        out_file.write(file_data)

    # Parse RINEX file
    rinex_obs = RINEXData(local_file)

    # Attempt to get Broadcast Navigation file from archive
    nav_file = ""
    
    # Hatanaka decompress RINEX file if needed 
    if rinex_obs.compressed == True:
        rinex_obs.local_file = hatanaka_decompress(rinex_obs.local_file)

    # Generate an Anubis XML config file
    anubis_config, result_file = generateQCConfig(
        rinex_obs, nav_file, local_path)

    # Run Anubis with the generated config file as input
    anubis = Executable('lib/executables/anubis-2.1.3')
    anubis_log = anubis.run('-x {}'.format(anubis_config))
    
    # Following section is closed as full header check is not possible due to issues
    
    if anubis.returncode > 0:
        print('Anubis errored with return code {}: {}\n{}'.format(
            anubis.returncode, anubis.stderr, anubis.stdout))
    try: 
        # Parse results of Anubis
        if flag == "full":
          RFV=parseQCResult(result_file, key, 2) #for detail check and submit to elastic search
        else:
          RFV=parseQCResult(result_file, key, 1) #only metadata check
    except:
        raise
        
    

    # Delete tmp working space and Anubis copy to resolve Lambda disk 
    # space allocation issue
    shutil.rmtree(local_path)

    return JSEncoder().encode(RFV)

    
      
def qc_test_allFalse(file, context):
    RFV = RinexFileValidation(False,False,False)   
    return JSEncoder().encode(RFV)    

def qc_test_AntennaValidFalse(file, context):
    RFV = RinexFileValidation(False,True,True)   
    RFV.AntennaValidationParameters.isMarkerArpUpEcc= True
    RFV.AntennaValidationParameters.isMarkerArpNorthEcc= True
    RFV.AntennaValidationParameters.isMarkerArpEastEcc= True
    RFV.AntennaValidationParameters.isAntenna= True
    RFV.AntennaValidationParameters.isManufacturerSerialNumber=False
    return JSEncoder().encode(RFV)    
   
def qc_test_SiteFalse(file, context):
    RFV = RinexFileValidation(False,True,False)   
    return JSEncoder().encode(RFV)    

def qc_test_ReciverFalse(file, context):
    RFV = RinexFileValidation(True,True,False)   
    return JSEncoder().encode(RFV)    

def metFileTest(event, context):
    
    key = urllib.unquote_plus(
         event['Records'][0]['s3']['object']['key']).decode('utf8')
    bucket = event['Records'][0]['s3']['bucket']['name']	
    
    session_id = context.aws_request_id
    fname=key;
    
    local_path = '/tmp/{}'.format(session_id)
    #trim_whitespace = lambda a: ' '.join(str(a).split())
    if not os.path.exists(local_path):
        os.makedirs(local_path)
    try:
        response = S3.get_object(Bucket=bucket, Key=key)

    except Exception as err:
        # This should only fail more than once if permissions are incorrect
        print('Error: Failed to get object {} from bucket {}.'.format(key, bucket))
        raise err

    # Decompress Observation file and store locally
    filename, extension = os.path.splitext(os.path.basename(key))
    local_file = os.path.join(local_path, filename)
    
    
    file_data = zlib.decompress(response['Body'].read(), 15+32)
    with open(local_file, 'wb') as out_file:
        out_file.write(file_data)

    rinex_obs = RINEXData(local_file)

    metFileProcess(rinex_obs)
def metFileProcessTest(bucket,rinFile,session_id):
    
    local_path = '/tmp/{}'.format(session_id)

    if not os.path.exists(local_path):
        os.makedirs(local_path)
    try:
        response = S3.get_object(Bucket=bucket, Key=rinFile)

    except Exception as err:
        # This should only fail more than once if permissions are incorrect
        print('Error: Failed to get object {} from bucket {}.'.format(rinFile, bucket))
        raise err

    # Decompress Observation file and store locally
    filename, extension = os.path.splitext(os.path.basename(rinFile))
    local_file = os.path.join(local_path, filename)
    
    
    file_data = zlib.decompress(response['Body'].read(), 15+32)
    with open(local_file, 'wb') as out_file:
        out_file.write(file_data)

    rinex_obs = RINEXData(local_file)
    
def metFileProcess(rinex_obs):
    
    gpsRecord={} 
    metOBSType_type = (rinex_obs.header_fields['# / TYPES OF OBSERV'][0:60])
    numberOfObs=int(metOBSType_type[0:6])
    
    currentLine=rinex_obs.observations
    averageObservation =20 # for every 10 min one observation form the mean of lst 10
    N=1
       
    for currentLine in rinex_obs.observations.splitlines():
            
        if currentLine.strip() == "":
            continue;
        year = int(currentLine[1:3])
        if year < 80:
            year = 2000 + year
        else:
            1900 + year
        month = int(currentLine[4:6])
        day = int(currentLine[7:9])
        hour = int(currentLine[10:12])
        mm = int(currentLine[13:15])
        sec = float(currentLine[16:19])
        time = datetime.date(year, month, day).isoweekday() % 7 * 24 * 3600 + hour * 3600 + mm * 60 + sec
        
        gpsRecord["timestamp"] = datetime.datetime.strptime(currentLine[0:18].strip(), '%y %m %d %H %M %S')
        
        st=7
        obsLineST=18
        for x in range(0, numberOfObs):
            if metOBSType_type[st:st+5].strip() in gpsRecord:
                gpsRecord[metOBSType_type[st:st+5].strip()] = gpsRecord[metOBSType_type[st:st+5].strip()]+float(currentLine[obsLineST:obsLineST+7])   # single-item list
                obsLineST=obsLineST+7
                st=st+6
            else:
                gpsRecord[metOBSType_type[st:st+5].strip()] = float(currentLine[obsLineST:obsLineST+7])   # single-item list
                obsLineST=obsLineST+7
                st=st+6

        if N==averageObservation:
            st=7
            obsLineST=18
            for x in range(0, numberOfObs):
                if metOBSType_type[st:st+5].strip() in gpsRecord:
                    gpsRecord[metOBSType_type[st:st+5].strip()] = round(float(gpsRecord[metOBSType_type[st:st+5].strip()]/averageObservation),3)   # single-item list
                    obsLineST=obsLineST+7
                    st=st+6
                else:
                    print("ERROR 2 {} {}".format(metOBSType_type,currentLine[st:st+5].strip()))
            
            gpsRecord["file_type"] = 'daily'
            gpsRecord["filename"] = rinex_obs.file_name
            gpsRecord["site_id"] = rinex_obs.marker_name
                
                
            gpsRecord_id = '{}{}{}'.format(gpsRecord['site_id'], gpsRecord['timestamp'],gpsRecord['file_type'])

            submitElasticSearchCluster('metData',gpsRecord_id,gpsRecord)
            N=1
            # reinitialize
            gpsRecord={}    
            st=7
        else:
            N=N+1
                    

def submitElasticSearchCluster(es_index,doc_id,doc):
    # Create authenticated connection to Elasticsearch cluster
    es_host = 'search-rinexqc-iy5n6isnfxolchh77wedwqwyh4.ap-southeast-2.es.amazonaws.com'
    #es_host = 'search-gnss-datacenter-es-2a65hbml7jlcthde6few3g7yxi.ap-southeast-2.es.amazonaws.com'
    es_index = 'quality_metrics'

    cred = boto3.session.Session().get_credentials()
    auth = AWSRequestsAuth(
        aws_access_key=cred.access_key,
        aws_secret_access_key=cred.secret_key,
        aws_token=cred.token,
        aws_host=es_host,
        aws_region='ap-southeast-2',
        aws_service='es')

    es_client = Elasticsearch(
        host=es_host,
        port=80,
        connection_class=RequestsHttpConnection,
        http_auth=auth)
    
    es_client.index(
        index=es_index, doc_type=doc['file_type'], body=doc, id=doc_id)
        

def isoTimetotrunckatedTime(dtime):
    dtime = datetime.datetime.strptime(dtime, '%Y-%m-%dT%H:%M:%S')
    d_truncated = datetime.date(dtime.year, dtime.month, dtime.day)
    return d_truncated.strftime("%Y-%m-%d")     
    
    
def fromatJSONObjecttoQC(data):
    sitelogTruth={'receiver':{},'antenna':{}}
    for keys in data['_embedded']['setups']:
        for key in keys['equipmentInUse']:
            if ('gnss receiver' in (key['content']['id']['equipmentType'])):
                sitelogTruth['receiver']['firmwareVersion']                 =  key['content']['configuration']['firmwareVersion']
                sitelogTruth['receiver']['receiverName']                    =  key['content']['id']['type']
                sitelogTruth['receiver']['manufacturerSerialNumber']        =  key['content']['id']['serialNumber']
            if ('gnss antenna' in (key['content']['id']['equipmentType'])):
                sitelogTruth['antenna']['markerArpUpEcc']               =   key['content']['configuration']['markerArpUpEccentricity']
                sitelogTruth['antenna']['markerArpNorthEcc']            =   key['content']['configuration']['markerArpNorthEccentricity']
                sitelogTruth['antenna']['markerArpEastEcc']             =   key['content']['configuration']['markerArpEastEccentricity']
                sitelogTruth['antenna']['manufacturerserialnumber']     =   key['content']['id']['serialNumber']
                sitelogTruth['antenna']['type']                         =   key['content']['id']['type']
            
    return sitelogTruth
