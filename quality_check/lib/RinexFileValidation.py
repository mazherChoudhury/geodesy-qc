import datetime
import os
import re
from collections import defaultdict
from json import JSONEncoder

class RinexFileValidation():
    # isAntennaValid,isSiteValid,isReceiverValid all boolean 
    def __init__(self,isAntennaValid,isSiteValid,isReceiverValid):
        """Initialize RinexFileValidation object

        Creates RinexFileValidation object when called./

        Input:
            local_file  Local path to dowloaded RINEX file

        Attributes generated:
            RinexFileValidation object that contains
            
        """
        self.isAntennaValid  = isAntennaValid  # 
        self.isSiteValid     = isSiteValid  #
        self.isReceiverValid = isReceiverValid  #
        #self.isDataValid     = False # future use if we want to do position validation   
        
        self.ReceiverValidationParameters = self.ReceiverValidationParameters(isReceiverValid)
        self.SiteValidationParameters = self.SiteValidationParameters(isSiteValid)
        self.AntennaValidationParameters = self.AntennaValidationParameters(isAntennaValid)
        
    class ReceiverValidationParameters():
        def __init__(self,val):
            self.isFirmwareValid     = val 
            self.isGnssReceiverNameValid     = val 
            self.isManufacturerSerialNumberNumberValid     = val 
 
    class SiteValidationParameters():
        def __init__(self,val):
            self.isSiteAgencyValid   = val
            self.isSite4CharNameValid = val
            self.isSiteIERSDOMESNumberValid        = val;
            self.isSiteCartesianPositionXValid     = val;
            self.isSiteCartesianPositionYValid     = val;
            self.isSiteCartesianPositionZValid     = val;
            
    class AntennaValidationParameters():
        def __init__(self,val):
            self.isMarkerArpUpEcc    = val
            self.isMarkerArpNorthEcc = val
            self.isMarkerArpEastEcc  = val
            self.isAntennaName       = val
            self.isManufacturerSerialNumber = val
            
 
