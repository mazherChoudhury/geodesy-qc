#!/usr/bin/env bash

scriptDir=$(readlink -f "${BASH_SOURCE[0]%/*}")

pushd "$scriptDir"

if [ ! -e ./python-env ]; then
    virtualenv python-env
fi

if [ "${VIRTUAL_ENV}" != "${scriptDir}"/python-env ]; then
    . python-env/bin/activate
fi

pip install . --upgrade

sitePackages=$(pip show requests | grep ^Location: | cut -f2 -d: | sed 's/^ //')

(cd "$sitePackages" && zip "$scriptDir"/quality_check.zip -r * \
        -x *.py* pip*\* setuptools*\* wheel*\* pkg_resources*\* __pycache__*\*)


# download source code of anubis
aws s3 cp s3://geodesy-archive-lambda-source-dev/anubis.zip  anubis.zip

# unzip source code
unzip anubis.zip -d lib/executables/



zip -r quality_check.zip quality_check.py anubis_base.cfg
zip -r quality_check.zip lib/ 


