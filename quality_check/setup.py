"Setup.py for Submit Rinex File Authorizer"

from setuptools import setup

setup(
    name='geodesy-qc',
    version='0.0.0',
    license='Creative Commons 4.0',
    install_requires=[
        'future',
        'elasticsearch',
        'requests',
        'urllib3',
        'beautifulsoup4'
    ],
)
